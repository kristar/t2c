# 简介

t2c 是一个根据模版生成代码的工具。

现在很多项目使用的代码生成器都是根据数据库表生成 MVC 代码的。但是我不希望一个代码生成器需要连接数据库才能使用。而且我需要的是一次性生成多个文件。

# 使用

```shell
$ t2c # 默认使用 template 目录下的模版，并生成文件到 template/out
$ t2c -t demo # 指定使用 demo 目录下的模版
```

## 配置文件

模版目录下需要有一个 env.json


```json
{
  "Env": {
    "Author": "kris"
  },
  "Map": {
    "javaToMySQL": {
      "Boolean" : "tinyint(1)",
      "Long": "bigint(20)"
    }
  },
  "Templates": [
    {
      "Path": "demo.txt",
      "Name": "demo.tpl"
    },
    {
      "Path": "{{.Author}}.txt",
      "Name": "demo.tpl"
    }
  ]
}
```

上面的配置，会在 template/out 目录下生成 demo.txt 和 kris.txt 两个文件。

注意：Env 的变量必须开头大写。
