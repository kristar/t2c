package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var testStrs = []string{
	"BankAccount",
	"Bank Account",
	"BANK ACCOUNT",
	"BANK_ACCOUNT",
	"BANK-ACCOUNT",
	"bankAccount",
	"bank account",
}

func TestCamelToPascar(t *testing.T) {
	assert.Equal(t, "BankAccount", CamelToPascar("bankAccount"))
}

func TestPascarToCamel(t *testing.T) {
	assert.Equal(t, "bankAccount", PascarToCamel("BankAccount"))
}
